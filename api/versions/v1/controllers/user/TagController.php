<?php

namespace api\versions\v1\controllers;

use Yii;
use api\versions\v1\controllers\tag\IndexAction;
use api\versions\v1\controllers\tag\CreateAction;
use api\versions\v1\controllers\tag\DeleteAction;
use common\models\Tag;
use common\overrides\rest\ActiveController;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

/**
 * LocationController implements the CRUD actions for Location model.
 */
class CategoryController extends ActiveController
{
    public $modelClass = Tag::class;
    
    const ACTION_INDEX = 'index';
    const ACTION_ASSIGN = 'assign';
    const ACTION_DELETE = 'delete';
    
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            self::ACTION_INDEX => ['class' => IndexAction::class],
            self::ACTION_ASSIGN => ['class' => AssignAction::class],
            self::ACTION_DELETE => ['class' => DeleteAction::class]
        ]);
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'authenticator' => [
                'except' => [
                    self::ACTION_INDEX,
                    self::ACTION_VIEW
                ], // pass authorization
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            self::ACTION_INDEX,
                            self::ACTION_ASSIGN
                        ],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            self::ACTION_DELETE
                        ],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }

    /**
     * Finds the Tag model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Tag the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tag::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Tag ID#'.$id.' not found.');
        }
    }
    
    /**
     * Finds the Tag model based on its name value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Tag|null the loaded model
     */
    protected function findModelByName($name)
    {
        return Tag::findOne(['name' => $name]);
    }
}
