<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace api\versions\v1\controllers\user;

use Yii;
use yii\web\ServerErrorHttpException;

/**
 * This action implemented to demonstrate the receipt of the token.
 * Do not use it on production systems.
 * @return string AuthKey or model with errors
 */
class DeleteAction extends \yii\rest\DeleteAction
{
    /**
     * Deletes a model.
     * @param mixed $id id of the model to be deleted.
     * @throws ServerErrorHttpException on failure.
     */
    public function run($id)
    {
        throw new ServerErrorHttpException('Deleted action is not implemented');
    }
}
