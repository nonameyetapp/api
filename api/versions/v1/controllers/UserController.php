<?php
namespace api\versions\v1\controllers;

use api\versions\v1\controllers\user\social\FacebookAuthAction;
use api\versions\v1\controllers\user\social\GoogleAuthAction;
use Yii;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use common\models\base\SignupForm;
use common\models\base\LoginForm;
use common\overrides\rest\ActiveController;
use api\versions\v1\models\User;
use api\versions\v1\controllers\user\RegistrationAction;
use api\versions\v1\controllers\user\LoginAction;
use api\versions\v1\controllers\user\DeleteAction;

/**
 * Class UserController
 *
 * @package api\versions\v1\controllers
 */
class UserController extends ActiveController
{
    public $modelClass = User::class;

    const ACTION_FACEBOOK_AUTH = 'facebook-auth';
    const ACTION_GOOGLE_AUTH = 'google-auth';
    const ACTION_LOGIN = 'login';
    const ACTION_CURRENT = 'current';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'authenticator' => [
                'except' => [
                    self::ACTION_CREATE,
                    self::ACTION_LOGIN,
                    self::ACTION_FACEBOOK_AUTH,
                    self::ACTION_GOOGLE_AUTH,
                ], // pass authorization
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            self::ACTION_CREATE,
                            self::ACTION_LOGIN,
                            self::ACTION_FACEBOOK_AUTH,
                            self::ACTION_GOOGLE_AUTH
                        ],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            self::ACTION_INDEX,
                            self::ACTION_VIEW,
                            self::ACTION_UPDATE,
                            self::ACTION_DELETE,
                            self::ACTION_CURRENT
                        ],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return array
     */
    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            // Create new user action using frontend SignupForm model.
            self::ACTION_CREATE => [
                'class' => RegistrationAction::class,
                'modelClass' => SignupForm::class,
            ],
            // Login user action using LoginForm model.
            self::ACTION_LOGIN => [
                'class' => LoginAction::class,
                'modelClass' => LoginForm::class,
            ],
            // Delete user action.
            self::ACTION_DELETE => [
                'class' => DeleteAction::class,
            ],

            self::ACTION_FACEBOOK_AUTH => FacebookAuthAction::class,
            self::ACTION_GOOGLE_AUTH => GoogleAuthAction::class,
        ]);
    }

    /**
     * @return null|\yii\web\IdentityInterface
     */
    public function actionCurrent()
    {
        return Yii::$app->user->identity;
    }
}
