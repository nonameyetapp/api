<?php

namespace api\versions\v1\controllers\category;

use Yii;

/**
 * View an existing Location model.
 * If model does not exist, not found exception will be thrown.
 * @param integer $id
 * @return boolean
 */
class ViewAction extends \yii\rest\ViewAction
{
    /**
     * Displays a single model.
     * @param integer $id
     * @return Category
     */
    public function run($id)
    {
        return $this->findModel($id);
    }
}
