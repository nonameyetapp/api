<?php

namespace api\versions\v1\controllers\category;

use Yii;

/**
 * Creates a new model.
 * If model is not created, errors should be returned.
 * @param integer $id
 * @return boolean
 */
class CreateAction extends \yii\rest\CreateAction
{
    /**
     * Updates an existing Category model.
     * @param integer $id
     * @return Category
     * 
     * @TODO implement nested set
     */
    public function run()
    {
        $model = new $this->modelClass();
        $post = Yii::$app->request->post();
        $model->setAttributes($post);
        $model->save();
        return $model;
    }
}
