<?php

namespace api\versions\v1\controllers\category;

use Yii;
use common\models\CategorySearch;

/**
 * View all models
 * @param integer $id
 * @return boolean
 */
class IndexAction extends \yii\rest\IndexAction
{
    /**
     * Lists all models.
     * @return Category[]
     */
    public function run()
    {
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $dataProvider->getModels();
    }
}
