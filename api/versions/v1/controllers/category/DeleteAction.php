<?php

namespace api\versions\v1\controllers\category;

use Yii;

/**
 * Deletes an existing model.
 * If deletion is successful, the browser will be redirected to the 'index' page.
 * @param integer $id
 * @return boolean
 */
class DeleteAction extends \yii\rest\DeleteAction
{
    /**
     * Deletes a model.
     * @param integer $id id of the model to be deleted.
     * @throws ServerErrorHttpException on failure.
     */
    public function run($id)
    {
        /** @var $model \common\models\Category */
        $model = $this->findModel($id);
        return $model->delete();
    }
}
