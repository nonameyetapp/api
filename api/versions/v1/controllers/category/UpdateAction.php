<?php

namespace api\versions\v1\controllers\category;

use Yii;

/**
 * Updates an existing model.
 * If update is successful, model will be returned. Otherwise errors will be displayed.
 * @param integer $id
 * @return boolean
 */
class UpdateAction extends \yii\rest\UpdateAction
{
    /**
     * Deletes a model.
     * @param integer $id id of the model to be deleted.
     * @throws ServerErrorHttpException on failure.
     */
    public function run($id)
    {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();
        $model->setAttributes($post);
        $model->save();
        return $model;
    }
}
