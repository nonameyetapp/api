<?php

namespace api\versions\v1\controllers;

use Yii;
use api\versions\v1\controllers\category\IndexAction;
use api\versions\v1\controllers\category\ViewAction;
use api\versions\v1\controllers\category\CreateAction;
use api\versions\v1\controllers\category\UpdateAction;
use api\versions\v1\controllers\category\DeleteAction;
use common\models\Category;
use common\overrides\rest\ActiveController;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

/**
 * LocationController implements the CRUD actions for Location model.
 */
class CategoryController extends ActiveController
{
    public $modelClass = Category::class;
    
    const ACTION_INDEX = 'index';
    const ACTION_VIEW = 'view';
    const ACTION_CREATE = 'create';
    const ACTION_UPDATE = 'update';
    const ACTION_DELETE = 'delete';
    
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            self::ACTION_INDEX => ['class' => IndexAction::class],
            self::ACTION_VIEW => ['class' => ViewAction::class],
            self::ACTION_CREATE => ['class' => CreateAction::class],
            self::ACTION_UPDATE => ['class' => UpdateAction::class],
            self::ACTION_DELETE => ['class' => DeleteAction::class]
        ]);
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'authenticator' => [
                'except' => [
                    self::ACTION_INDEX,
                    self::ACTION_VIEW
                ], // pass authorization
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            self::ACTION_INDEX,
                            self::ACTION_VIEW
                        ],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            self::ACTION_CREATE,
                            self::ACTION_UPDATE,
                            self::ACTION_DELETE
                        ],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Category ID#'.$id.' not found.');
        }
    }
}
