<?php

use api\versions\v1\controllers\UserController;

return [
    'enablePrettyUrl' => true,
    'enableStrictParsing' => false,
    'showScriptName' => false,
    'rules' => [
        [
            'class' => 'common\overrides\rest\UrlRule',
            'controller' => ['v1/user'],
            'extraPatterns' => [
                'POST login' => 'login',
                'GET current' => 'current',
                'POST facebook-auth' => UserController::ACTION_FACEBOOK_AUTH,
            ],
            'pluralize' => true,
        ],
        [
            'class' => 'common\overrides\rest\UrlRule',
            'controller' => ['v1/location'],
            'extraPatterns' => [
                'GET <id:\d+>' => 'view',
                'POST /' => 'create',
                'POST <id:\d+>' => 'update',
                'DELETE <id:\d+>' => 'delete',
            ],
            'pluralize' => true,
        ],
        [
            'class' => 'common\overrides\rest\UrlRule',
            'controller' => ['v1/category'],
            'extraPatterns' => [
                'GET <id:\d+>' => 'view',
                'POST /' => 'create',
                'POST <id:\d+>' => 'update',
                'DELETE <id:\d+>' => 'delete',
            ],
            'pluralize' => true,
        ],
    ],
];