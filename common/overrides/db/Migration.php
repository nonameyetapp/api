<?php
namespace common\overrides\db;

use yii\db\Migration as BaseMigration;

/**
 * Class Migration
 * @package common\overrides\db
 */
class Migration extends BaseMigration
{

    public static $intType;
    public static $idType;
    public static $tableOptions = '';

    public function init()
    {
        parent::init();
        static::$tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            static::$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        static::$intType = 'BIGINT(20) UNSIGNED';
        static::$idType = static::$intType.' NOT NULL AUTO_INCREMENT PRIMARY KEY';
    }
}
