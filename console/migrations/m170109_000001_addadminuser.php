<?php

use yii\db\Migration;

class m170109_000001_addadminuser extends Migration
{
    public function up()
    {
        $this->insert(
            '{{%user}}',
            [
                'email' => 'admin@admin.com',
		'username' => 'admin',
                'password_hash' => \Yii::$app->security->generatePasswordHash('admin'),
                'auth_key' => 'PqWDT5DjNbo8V705pNWzQDGkFRdOr78V',
		'status' => 10,
		'role' => 20,
                'created_at' => time(),
                'updated_at' => time(),
            ]
        );
    }

    public function down()
    {
        $this->delete('{{%user}}', ['username' => 'admin']);
    }
}
