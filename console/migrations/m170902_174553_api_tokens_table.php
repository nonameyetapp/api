<?php

use common\overrides\db\Migration;

class m170902_174553_api_tokens_table extends Migration
{
    const TABLE_USER = '{{%user}}';
    const TABLE_USER_TOKEN = '{{%user_token}}';

    /**
     * Tokens for rest api
     */
    public function up()
    {

        $this->createTable(self::TABLE_USER_TOKEN, [
            'id' => static::$idType,
            'user_id' => static::$intType,
            'token' => $this->string(128)->notNull(),
            'verify_ip' => $this->boolean()->defaultValue(false),
            'user_ip' => $this->string(46),
            'user_agent' => $this->text(),
            'frozen_expire'  => $this->boolean()->defaultValue(true),
            'created_at' => $this->dateTime(),
            'expired_at'  => $this->dateTime(),
        ], self::$tableOptions);

        $this->addForeignKey('fk_token_user', self::TABLE_USER_TOKEN, 'user_id', self::TABLE_USER, 'id', 'CASCADE');
        $this->createIndex('i_user_token', self::TABLE_USER_TOKEN, 'token');
        $this->createIndex('i_user_token_expired', self::TABLE_USER_TOKEN, ['token', 'expired_at']);
    }

    public function down()
    {
        $this->dropIndex('i_user_token_expired', self::TABLE_USER_TOKEN);
        $this->dropIndex('i_user_token', self::TABLE_USER_TOKEN);
        $this->dropForeignKey('fk_token_user', self::TABLE_USER_TOKEN);
        $this->dropTable(self::TABLE_USER_TOKEN);
    }
}
